import Highcharts from 'highcharts';
import moment from 'moment'
import numeral from 'numeral'
import SimpleSchema from 'simpl-schema'
SimpleSchema.extendOptions(['autoform']);
//import Chartjs from 'chart.js'
import Tabular from 'meteor/aldeed:tabular';
import { Template } from 'meteor/templating';
import { Meteor } from 'meteor/meteor';

// COLLECTIONS
portfolio = new Mongo.Collection("portfolio");
outputjsondt = new Mongo.Collection("outputjsondt");
outputjsonhc = new Mongo.Collection("outputjsonhc");
outputmisc = new Mongo.Collection("outputmisc");
outputcsv = new Mongo.Collection("outputcsv");
profile = new Mongo.Collection("profile");
transaction = new Mongo.Collection("transaction");

// ALLOWS - only allow actions if user is logged in
portfolio.allow({
	insert: function(userId, doc) {
		return userId;
	},
	update: function(userId, doc, fields, modifier) {
		return doc.portfolioowner === userId;
	},
	remove: function(userId, doc) {
		return doc.portfolioowner === userId;
	}
});
transaction.allow({
	insert: function(userId, doc) {
		return userId;
	},
	update: function(userId, doc, fields, modifier) {
		return doc.portfolioowner === userId;
	},
	remove: function(userId, doc) {
		return doc.portfolioowner === userId;
	}
});
// IRON ROUTER
Router.route('/', {
	name: 'home',
	template: 'home'
});
Router.route('/portfolio-summary', {
	name: 'portfoliosummary',
	template: 'portfoliosummary',
	waitOn: function() {
		return [Meteor.subscribe('all_portfolio')];
	},
	onBeforeAction: function() {
		var currentUser = Meteor.userId();
		if (currentUser) {
			this.next();
		} else {
			Router.go('/sign-in');
		}
	}
});
Router.route('/portfolio-detail/:portfolioobjectid', {
	name: 'portfoliodetail',
	template: 'portfoliodetail',
	waitOn: function() {
		var portfolioobjectid = this.params.portfolioobjectid;
		return [Meteor.subscribe('one_portfolio', portfolioobjectid),
			/*Meteor.subscribe('outputjsondt', portfolioobjectid),*/
			Meteor.subscribe('outputjsonhc', portfolioobjectid),
			Meteor.subscribe('outputmisc', portfolioobjectid)
			/*,Meteor.subscribe('transaction', {'portfolio': portfolioobjectid})*/
		];
	},
	data: function() {
		portfolioobjectid = this.params.portfolioobjectid;
		return portfolioobjectid;
	},
	onBeforeAction: function() {
		var currentUser = Meteor.userId();
		if (currentUser) {
			this.next();
		} else {
			Router.go('/sign-in');
		}
	}
});
Router.route('/documentation', {
	name: 'documentation',
	template: 'documentation'
});
Router.route('/securitiesdatabase', {
	name: 'securitiesdatabase',
	template: 'securitiesdatabase'
});
Router.configure({
	layoutTemplate: 'Layout',
	yieldTemplates: {
		navigation: {
			to: 'navigation'
		},
		pageheader: {
			to: 'pageheader'
		},
		autoformModals: {
			to: 'autoformModals'
		},
		footer: {
			to: 'footer'
		}
	}
});
//this MUST be run before configureRoute
AccountsTemplates.configure({
	defaultLayout: 'Layout',
	showForgotPasswordLink: true,
	enablePasswordChange: true
});
//Routes
AccountsTemplates.configureRoute('changePwd');
AccountsTemplates.configureRoute('resetPwd');
AccountsTemplates.configureRoute('forgotPwd', {
	redirect: '/sign-in'
});
AccountsTemplates.configureRoute('signIn', {
	name: 'signin',
	path: '/sign-in',
	redirect: function() {
		var user = Meteor.user();
		if (user) Router.go('/portfolio-summary');
	}
});
AccountsTemplates.configureRoute('signUp', {
	name: 'register',
	path: '/register',
	redirect: function() {
		var user = Meteor.user();
		if (user) Router.go('/portfolio-summary');
	}
});
AccountsTemplates.configureRoute('verifyEmail');

/*
function mkLabelValue(v) {
		console.log(v)
		return {
			'value': v.ticker
		};
	}
*/

// SCHEMAS
var Schemas = {};
Schemas.transaction = new SimpleSchema({
	date: {
		type: Date,
		label: "Date",
		autoform: {
			type: "bootstrap-datepicker",
			datePickerOptions: function() {
				var options = {
					format: "yyyy-mm-dd",
					todayBtn: "linked",
					autoclose: "true"
				}
				return options;
			}
		}
	},
	ticker: {
		type: String,
		label: "Ticker",
		max: 200,
		autoform: {
			type: "typeahead",
			afFieldInput: {
				typeaheadOptions: {},
				typeaheadDatasets: {
					'source': function(query, process) {
						Meteor.call('search', query, {}, function(err, res) {
							if (err) {
								console.log(err);
								return;
							}
							items = res.map(function(v) {
								return {
									'value': v.ticker,
									'name': v.name,
									'exchange': v.exchange
								};
							});
							process(items);
						});
					},
					'templates': {
						'suggestion': function(v) {
							return '<p class="newtickertypeahead-ticker">' + v.value + '</p><p class="newtickertypeahead-exchange">' + v.exchange + '</p><p class="newtickertypeahead-name">' + v.name + '</p>';
						}
					}
					//v.value.concat(" - ", v.name, " - ", v.exchange); }}
				}
			}
		}
	},
	quantity: {
		type: Number,
		label: "Quantity"
	},
	price: {
		type: Number,
		label: "Price",
		min: 0
	},
	commission: {
		type: Number,
		label: "Commission",
		min: 0
	},
	fxrate: {
		type: Number,
		label: "FX Rate",
		min: 0
	},
	portfolio: {
		type: String
	},
	portfolioowner: {
		type: String,
		autoValue: function() {
			if (this.isInsert) {
				return this.userId;
			} else if (this.isUpsert) {
				return {
					$setOnInsert: this.userId
				};
			} else {
				this.unset();
			}
		}
	},
	entered: {
		type: Date,
		autoValue: function() {
			if (this.isInsert) {
				return new Date();
			} else if (this.isUpsert) {
				return {
					$setOnInsert: new Date()
				};
			} else {
				this.unset();
			}
		}
	},
	enteredby: {
		type: String,
		autoValue: function() {
			return this.userId
		}
	}
});
transaction.attachSchema(Schemas.transaction);
Schemas.portfolio = new SimpleSchema({
	name: {
		type: String,
		label: "Name",
		max: 200
	},
	currency: {
		type: String,
		label: "Base Currency",
		max: 3,
		autoform: {
			options: [{
				label: 'Argentinian peso - ARS',
				value: 'ARS'
			}, {
				label: 'Australian dollar - AUD',
				value: 'AUD'
			}, {
				label: 'Brazilian real - BRL',
				value: 'BRL'
			}, {
				label: 'Canadian dollar - CAD',
				value: 'CAD'
			}, {
				label: 'Danish krone - DKK',
				value: 'DKK'
			}, {
				label: 'Euro - EUR',
				value: 'EUR'
			}, {
				label: 'Hong Kong dollar - HKD',
				value: 'HKD'
			}, {
				label: 'Malaysian ringgit - MYR',
				value: 'MYR'
			}, {
				label: 'Mexican peso - MXN',
				value: 'MXN'
			}, {
				label: 'New Zealand dollar - NZD',
				value: 'NZD'
			}, {
				label: 'Norwegian krone - NOK',
				value: 'NOK'
			}, {
				label: 'Pound sterling - GBP',
				value: 'GBP'
			}, {
				label: 'Russian ruble - RUB',
				value: 'RUB'
			}, {
				label: 'Singapore dollar - SGD',
				value: 'SGD'
			}, {
				label: 'South Korean won - KRW',
				value: 'KRW'
			}, {
				label: 'Swedish krona - SEK',
				value: 'SEK'
			}, {
				label: 'Swiss franc - CHF',
				value: 'CHF'
			}, {
				label: 'United States dollar - USD',
				value: 'USD'
			}]
		}
	},
	benchmark: {
		type: Array,
		label: "Benchmarks",
		maxCount: 5,
		optional: true,
		autoform: {
			options: [{
				label: 'AEX Amsterdam Index (Netherlands) - ^AEX',
				value: '^AEX'
			}, {
				label: 'Bovespa Index (Brazil) - ^BVSP',
				value: '^BVSP'
			}, {
				label: 'CAC 40 Index (France) - ^FCHI',
				value: '^FCHI'
			}, {
				label: 'DAX Index (Germany) - ^GDAXI',
				value: '^GDAXI'
			}, {
				label: 'Euronext BEL-20 Index (Belgium) - ^BFX',
				value: '^BFX'
			}, {
				label: 'FTSE 100 (United Kingdom) - ^FTSE',
				value: '^FTSE'
			}, {
				label: 'FTSE Bursa Malaysia KLCI Index (Malaysia) - ^KLSE',
				value: '^KLSE'
			}, {
				label: 'FTSE MIB Index (Italy) - FTSEMIB.MI',
				value: 'FTSEMIB.MI'
			}, {
				label: 'FTSE/Athex Large Cap Index (Greece) - FTSE.AT',
				value: 'FTSE.AT'
			}, {
				label: 'Hang Seng Index (Hong Kong) - ^HSI',
				value: '^HSI'
			}, {
				label: 'IPC Index (Mexico) - ^MXX',
				value: '^MXX'
			}, {
				label: 'ISEQ 20 Price Index (Ireland) - ^IETP',
				value: '^IETP'
			}, {
				label: 'KOSPI Composite Index (South Korea) - ^KS11',
				value: '^KS11'
			}, {
				label: 'MERVAL Index (Argentina) - ^MERV',
				value: '^MERV'
			}, {
				label: 'MICEX 10 Index (Russia) - MICEX10INDEX.ME',
				value: 'MICEX10INDEX.ME'
			}, {
				label: 'NZSE 50 (New Zealand) - ^NZ50',
				value: '^NZ50'
			}, {
				label: 'OMX Copenhagen 20 Index (Denmark) - ^OMXC20_CO',
				value: '^OMXC20_CO'
			}, {
				label: 'OMXS 30 Index (Sweden) - ^OMX',
				value: '^OMX'
			}, {
				label: 'Oslo OBX Index (Norway) - OBX.OL',
				value: 'OBX.OL'
			}, {
				label: 'S&P 100 Index (United States) - ^OEX',
				value: '^OEX'
			}, {
				label: 'S&P 500 Index (United States) - ^GSPC',
				value: '^GSPC'
			}, {
				label: 'S&P TSX Composite Index (Canada) - ^GSPTSE',
				value: '^GSPTSE'
			}, {
				label: 'S&P/ASX 200 Index (Australia) - ^AXJO',
				value: '^AXJO'
			}, {
				label: 'Straits Times Index (Singapore) - ^STI',
				value: '^STI'
			}, {
				label: 'Swiss Market Index (Switzerland) - ^SSMI',
				value: '^SSMI'
			}]
		}
	},
	'benchmark.$': {type: String},
	portfolioowner: {
		type: String,
		autoValue: function() {
			if (this.isInsert) {
				return this.userId;
			} else if (this.isUpsert) {
				return {
					$setOnInsert: this.userId
				};
			} else {
				this.unset();
			}
		}
	},
	entered: {
		type: Date,
		autoValue: function() {
			if (this.isInsert) {
				return new Date();
			} else if (this.isUpsert) {
				return {
					$setOnInsert: new Date()
				};
			} else {
				this.unset();
			}
		}
	},
	enteredby: {
		type: String,
		autoValue: function() {
			return this.userId
		}
	},
	status: {
		type: String,
		autoValue: function() {
			return "NEW"
		}
	}
});
portfolio.attachSchema(Schemas.portfolio);

// to run on the server
if (Meteor.isServer) {
    var celery = require('node-celery'),
        client = celery.createClient({
            CELERY_BROKER_URL: process.env.CELERY_BROKER_URL_PROD || 'amqp://guest:guest@localhost:5672',
            CELERY_RESULT_BACKEND: 'amqp://'
        });
    client.on('error', function(err) {
        console.log(err);
    });
    client.on('connect', function() {});
	// PUBLISH
	Meteor.publish("all_portfolio", function() {
		return portfolio.find({
			'portfolioowner': this.userId
		});
	});
	Meteor.publish("one_portfolio", function(portfolioobjectid) {
		return portfolio.find({
			'_id': portfolioobjectid,
			'portfolioowner': this.userId
		});
	});
	/*
	Meteor.publish("outputjsondt", function() {
		var portfolioobjectids = portfolio.find({
			'portfolioowner': this.userId
		}).map(function(u) {
			return u._id;
		});
		return outputjsondt.find({
			'portfolio': {
				$in: portfolioobjectids
			}
		});
	});*/
	Meteor.publish("outputjsonhc", function(portfolioobjectid) {
		return outputjsonhc.find({
			'portfolio': portfolioobjectid,
			'portfolioowner': this.userId
		});
	});
	Meteor.publish("outputmisc", function(portfolioobjectid) {
		return outputmisc.find({
			'portfolio': portfolioobjectid,
			'portfolioowner': this.userId
		});
	});
	Meteor.publish("outputcsv", function(portfolioobjectid, name) {
		return outputcsv.find({
			'portfolio': portfolioobjectid,
			'portfolioowner': this.userId,
			'name': name
		});
	});
	/*Meteor.publish("eqprofile", function() {
		return eqprofile.find();
	});
	Meteor.publish("transaction", function(portfolioobjectid) {
		return transaction.find({
		    'portfolio': portfolioobjectid,
			'portfolioowner': this.userId
		});
	});*/
	//var runall = Celery.createTask('portfoliorobot_celery.tasks.runall')
	Meteor.methods({
		"start_robotics": function(x) {
			console.log(x, 'PENDING');
			portfolio.update({
				'_id': x
			}, {
				$set: {
					'status': 'PENDING'
				}
			}, {
				getAutoValues: false
			}); //validate: false,
           Meteor.defer(function() {
            client.call('portfoliorobot_celery.tasks.runall', [x], function(result) {
                //console.log(result);
                client.end();

        });
        });
		},
		'parseupload' (data) {
			for (let i = 0; i < data.length; i++) {
				let item = data[i];
				transaction.insert(item);
			}
		},
		'search': function(query, options) {
			options = options || {};
			// guard against client-side DOS: hard limit to 50
			if (options.limit) {
				options.limit = Math.min(10, Math.abs(options.limit));
			} else {
				options.limit = 10;
			}
			// TODO fix regexp to support multiple tokens
			var regex = new RegExp(query, 'i');
			return profile.find({
				$or: [{
					'ticker': {
						$regex: regex
					}
				}, {
					'name': {
						$regex: regex
					}
				}]
			}, options).fetch();
		}
	});
}
// to run on the client
if (Meteor.isClient) {
import { $ } from 'meteor/jquery';
import dataTablesBootstrap from 'datatables.net-bs';
import 'datatables.net-bs/css/dataTables.bootstrap.css';
dataTablesBootstrap(window, $);
//let datatablesnetbs = require( 'datatables.net-bs' )( window, $ );
//let datatablesnetresponsive = require( 'datatables.net-responsive' )( window, $ );
//let datatablesnetresponsivebs = require( 'datatables.net-responsive-bs' )( window, $ );

	// HELPERS
	Template.navigation.helpers({
		activeIfTemplateIs: function(template) {
			var currentRoute = Router.current();
			return currentRoute && template === currentRoute.lookupTemplate() ? 'active' : '';
		}
	});
	Template.pageheader.helpers({
		'portfolioname': function() {
			return portfolio.findOne({
				'_id': Router.current().params.portfolioobjectid
			}).name;
		}
	});
	Template.portfoliosummary.helpers({
		'portfolio': function() {
			return portfolio.find();
		}
	});
	Template.portfoliodetail.helpers({
		pending() {
				var status = portfolio.findOne({
					'_id': Router.current().params.portfolioobjectid
				}).status
				if (status == 'PENDING') {
					return true;
				} else {
					return false;
				}
			},
			started() {
				var status = portfolio.findOne({
					'_id': Router.current().params.portfolioobjectid
				}).status
				if (status == 'STARTED') {
					return true;
				} else {
					return false;
				}
			},
			uploading() {
				return Template.instance().uploading.get();
			}, 'transaction': function() {
				return transaction.find();
			}, 'portfolio': function() {
				return portfolio.find({
					'_id': Router.current().params.portfolioobjectid
				});
			}, 'create_chart_allocation': function() {
				var seriesdata = outputjsonhc.findOne({
					'name': 'chart_allocation',
					'portfolio': Router.current().params.portfolioobjectid
				}).data;
				Meteor.defer(function() {
					Highcharts.chart('chart_allocation', {
					    chart: {type: 'pie'},
						series: seriesdata,
						title: {
							text: "Asset"
						},
						plotOptions: {
                        pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
						tooltip: {
							valueSuffix: ' %'
						},
						credits: {
							enabled: false
						}
					});
				});
			}, 'create_chart_currencyallocation': function() {
				var seriesdata = outputjsonhc.findOne({
					'name': 'chart_currencyallocation',
					'portfolio': Router.current().params.portfolioobjectid
				}).data;
				Meteor.defer(function() {
					Highcharts.chart('chart_currencyallocation', {
					    chart: {type: 'pie'},
						series: seriesdata,
						title: {
							text: "Currency"
						},
						            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
						tooltip: {
							valueSuffix: ' %'
						},
						credits: {
							enabled: false
						}
					});
				});
			}, 'create_chart_sectorallocation': function() {
				var seriesdata = outputjsonhc.findOne({
					'name': 'chart_sectorallocation',
					'portfolio': Router.current().params.portfolioobjectid
				}).data;
				Meteor.defer(function() {
					Highcharts.chart('chart_sectorallocation', {
					    chart: {type: 'pie'},
						series: seriesdata,
						title: {
							text: "Sector"
						},
						            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
						tooltip: {
							valueSuffix: ' %'
						},
						credits: {
							enabled: false
						}
					});
				});
			}, 'create_chart_industryallocation': function() {
				var seriesdata = outputjsonhc.findOne({
					'name': 'chart_industryallocation',
					'portfolio': Router.current().params.portfolioobjectid
				}).data;
				Meteor.defer(function() {
					Highcharts.chart('chart_industryallocation', {
					    chart: {type: 'pie'},
						series: seriesdata,
						title: {
							text: "Industry"
						},
						            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
						tooltip: {
							valueSuffix: ' %'
						},
						credits: {
							enabled: false
						}
					});
				});
				 },
				 /* Testing Chart.js
				 'create_test': function() {
                    var ctx = document.getElementById("myChart");
                     var myChart = new Chart.Line(ctx, {
                         data: seriesdata,
                         options: {
                             responsive: false,
                             animation: false,
                             scales: {
                                 xAxes: [{
                                     type: 'time'
                                 }]
                             },
                             tooltips: {
                                 mode: 'label'
                             },
                             hover: {
                                 mode: 'label'
                             }
                         }
                     })
				 })},*/

				 'create_p_bm_cumulativereturns': function() {
				var seriesdata = outputjsonhc.findOne({
					'name': 'p_bm_cumulativereturns',
					'portfolio': Router.current().params.portfolioobjectid
				}).data;
				Meteor.defer(function() {
					Highcharts.chart('p_bm_cumulativereturns', {
						series: seriesdata,
						title: {
							text: null
						},
						xAxis: {
							type: 'datetime',
							minTickInterval: 1000 * 60 * 60 * 24
						},
						yAxis: {
							title: {
								text: null
							},
							labels: {
								formatter: function() {
									return this.value + ' %';
								}
							}
						},
						tooltip: {
							shared: true,
							crosshairs: true,
							headerFormat: '<span style="font-size: 10px">{point.key:%Y-%m-%d}</span><br/>',
							valueSuffix: ' %'
						},
						credits: {
							enabled: false
						}
					});
				});
			}, 'create_eq_pnl_cumpct': function() {
				var seriesdata = outputjsonhc.findOne({
					'name': 'eq_pnl_cumpct',
					'portfolio': Router.current().params.portfolioobjectid
				}).data;
				Meteor.defer(function() {
					Highcharts.chart('eq_pnl_cumpct', {
						series: seriesdata,
						title: {
							text: null
						},
						xAxis: {
							type: 'datetime'
						},
						yAxis: {
							title: {
								text: null
							},
							labels: {
								formatter: function() {
									return this.value + ' %';
								}
							}
						},
						tooltip: {
							shared: true,
							crosshairs: true,
							headerFormat: '<span style="font-size: 10px">{point.key:%Y-%m-%d}</span><br/>',
							valueSuffix: ' %'
						},
						credits: {
							enabled: false
						}
					});
				});
			}, 'create_chart_currencymovements': function() {
			var seriesdata = outputjsonhc.findOne({
				'name': 'chart_currencymovements',
				'portfolio': Router.current().params.portfolioobjectid
			}).data
			Meteor.defer(function() {
				Highcharts.chart('chart_currencymovements', {
					series: seriesdata,
					title: {
						text: null
					},
					xAxis: {
						type: 'datetime'
					},
					yAxis: {
						title: {
							text: null
						}
					},
					tooltip: {
						shared: true,
						crosshairs: true,
						headerFormat: '<span style="font-size: 10px">{point.key:%Y-%m-%d}</span><br/>'
					},
					credits: {
						enabled: false
					}
				});
			});
		},
		'create_chart_currencyimpact': function() {
			var seriesdata = outputjsonhc.findOne({
				'name': 'chart_currencyimpact',
				'portfolio': Router.current().params.portfolioobjectid
			}).data;
			var xaxiscategories = outputmisc.findOne({
							'name': 'chart_currencyimpact_xaxis',
							'portfolio': Router.current().params.portfolioobjectid
						}).data;
			Meteor.defer(function() {
				Highcharts.chart('chart_currencyimpact', {
					chart: {
						type: 'column'
					},
					series: seriesdata,
					title: {
						text: null
					},
					xAxis: {
						categories: xaxiscategories
					},
					yAxis: {
						title: {
							text: null
						},
						/*stackLabels: {
							enabled: true,
							style: {
								fontWeight: 'bold',
								color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
							},
							formatter: function() {
								return this.total + ' %';
							}
						},*/
						labels: {
							formatter: function() {
								return this.value + ' %';
							}
						}
					},
					/*legend: {
						align: 'right',
						x: -10,
						verticalAlign: 'top',
						y: 10,
						floating: true,
						backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
						borderColor: '#CCC',
						borderWidth: 1,
						shadow: false
					},*/
					tooltip: {
						/*headerFormat: '<b>{point.x}</b><br/>',
						pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal} %',*/
						shared: true,
						valueSuffix: ' %'
					},
					/*plotOptions: {
						column: {
							stacking: 'normal',
							dataLabels: {
								enabled: true,
								color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
								style: {
									textShadow: '0 0 3px black'
								},
								formatter: function() {
									return this.y + ' %';
								}
							}
						}
					},*/
					credits: {
						enabled: false
					}
				});
			});
		},
		'table_aa_selector': function() {
			return {
				'name': 'table_aa',
				'portfolio': Router.current().params.portfolioobjectid,
				'portfolioowner': Meteor.userId()
			};
		},
		'table_analytics_selector': function() {
			return {
				'name': 'table_analytics',
				'portfolio': Router.current().params.portfolioobjectid,
				'portfolioowner': Meteor.userId()
			};
		},
		'table_monthlyreturns_selector': function() {
			return {
				'name': 'table_monthlyreturns',
				'portfolio': Router.current().params.portfolioobjectid,
				'portfolioowner': Meteor.userId()
			};
		},
		'table_ap_selector': function() {
			return {
				'name': 'table_ap',
				'portfolio': Router.current().params.portfolioobjectid,
				'portfolioowner': Meteor.userId()
			};
		},
		'table_transaction_selector': function() {
				return {
					'portfolio': Router.current().params.portfolioobjectid,
					'portfolioowner': Meteor.userId()
				};
			}
			/*, 'portfolioobjectid': function() {
            return Router.current().params.portfolioobjectid;
        }*/
	});
	Template.registerHelper('formatdate', function(date) {
		return moment(date).format('YYYY-MM-DD');
	});
	Template.registerHelper('formatnumbernodp', function(number) {
		return numeral(number).format('0,0');
	});
	Template.registerHelper('formatnumber2dp', function(number) {
		return numeral(number).format('0,0.00');
	});
	Template.registerHelper("equals", function(a, b) {
		return (a == b);
	});
	// EVENTS
	Template.navigation.events({
		'click #logout': function(event) {
			event.preventDefault();
			Meteor.logout();
			Router.go('/');
		}
	});
	Template.portfoliodetail.events({
		'click .download_button': function(event) {
			portfolioobjectid = Router.current().params.portfolioobjectid;
			csvrequest = event.currentTarget.getAttribute("csvrequest");
			Meteor.subscribe('outputcsv', portfolioobjectid, csvrequest, {
				onReady: function() {
					csv = outputcsv.findOne({
						'name': csvrequest
					}).data;
					var blob = new Blob([csv]);
					var a = window.document.createElement("a");
					a.href = window.URL.createObjectURL(blob, {
						type: "text/plain"
					});
					a.download = csvrequest.concat(".csv");
					a.click();
				}
			});
		},
		'click .start_button': function() {
			portfolioobjectid = Router.current().params.portfolioobjectid;
			Meteor.call('start_robotics', portfolioobjectid);
		},
		'change [name="uploadcsv"]' (event, template) {
			template.uploading.set(true);
			Papa.parse(event.target.files[0], {
				header: true,
				complete(results, file) {
					Meteor.call('parseupload', results.data, (error, response) => {
						if (error) {
							console.log(error.reason);
							template.uploading.set(false);
						} else {
							template.uploading.set(false);
							//Bert.alert( 'Upload complete!', 'success', 'growl-top-right' );
						}
					});
				}
			});
		}
	});
	// AUTOFORM HOOKS
	var transactionhooks = {
		before: {
			insert: function(doc) {
				doc.portfolio = Router.current().params.portfolioobjectid;
				return doc;
			}
		}
	};
	AutoForm.hooks({
		'newtransactionform': transactionhooks
	});
	// MISC
	Template.portfoliodetail.onCreated(() => {
		Template.instance().uploading = new ReactiveVar(false);
		//Template.instance().isempty = new ReactiveVar(false);
	});
}
// COMMON CODE - TABULAR TABLES
TabularTables = {};
TabularTables.table_aa = new Tabular.Table({
	name: 'table_aa',
	collection: outputjsondt,
	selector: function(userId) {
		return {
			'name': 'table_aa',
			'portfolioowner': userId
		};
	},
	responsive: true,
	columnDefs: [{
		responsivePriority: 1,
		targets: 0
	}, {
		responsivePriority: 2,
		targets: 6
	}, {
		responsivePriority: 3,
		targets: 5
	}],
	autoWidth: false,
	order: [
		[5, "desc"]
	],
	columns: [{
		data: 'Ticker',
		title: 'Ticker'
	}, {
		data: 'Name',
		title: 'Name'
	}, {
		data: 'Quantity',
		title: 'Quantity',
		render: function(val) {
			return numeral(val).format('0,0');
		}
	}, {
		data: 'Price',
		title: 'Price',
		render: function(val) {
			return numeral(val).format('0,0.00');
		}
	}, {
		data: 'MV (local)',
		title: 'MV (local)',
		render: function(val) {
			return numeral(val).format('0,0.00');
		}
	}, {
		data: 'MV (base)',
		title: 'MV (base)',
		render: function(val) {
			return numeral(val).format('0,0.00');
		}
	}, {
		data: 'Allocation',
		title: 'Allocation %',
		render: function(val) {
			return numeral(val).format('0.00');
		}
	}, {
		data: 'Currency',
		title: 'Currency'
	}, {
		data: 'Sector',
		title: 'Sector'
	}, {
		data: 'Industry',
		title: 'Industry'
	}]
});
TabularTables.table_analytics = new Tabular.Table({
	name: 'table_analytics',
	collection: outputjsondt,
	selector: function(userId) {
		return {
			'name': 'table_analytics',
			'portfolioowner': userId
		};
	},
	paging: false,
	limit: 100,
	info: false,
	searching: false,
	responsive: true,
	autoWidth: false,
	columns: [{
		data: 'Portfolio / benchmark',
		title: 'Portfolio / benchmark'
	}, {
		data: '1 day return',
		title: '1 day return %',
		render: function(val) {
			return numeral(val).format('0,0.00');
		}
	}, {
		data: '5 day return',
		title: '5 day return %',
		render: function(val) {
			return numeral(val).format('0,0.00');
		}
	}, {
		data: '1 month return',
		title: '1 month return %',
		render: function(val) {
			return numeral(val).format('0,0.00');
		}
	}, {
		data: '3 month return',
		title: '3 month return %',
		render: function(val) {
			return numeral(val).format('0,0.00');
		}
	}, {
		data: 'YTD return',
		title: 'YTD return %',
		render: function(val) {
			return numeral(val).format('0,0.00');
		}
	}, {
		data: '1 year return',
		title: '1 year return %',
		render: function(val) {
			return numeral(val).format('0,0.00');
		}
	}, {
		data: '3 year return',
		title: '3 year return %',
		render: function(val) {
			return numeral(val).format('0,0.00');
		}
	}, {
		data: '5 year return',
		title: '5 year return %',
		render: function(val) {
			return numeral(val).format('0,0.00');
		}
	}, {
		data: 'Annualised return',
		title: 'Annualised return since inception %',
		render: function(val) {
			return numeral(val).format('0,0.00');
		}
	}, {
		data: 'Annualised volatility',
		title: 'Annualised volatility %',
		render: function(val) {
			return numeral(val).format('0,0.00');
		}
	}, {
		data: 'Annualised sharpe',
		title: 'Annualised sharpe',
		render: function(val) {
			return numeral(val).format('0,0.00');
		}
	}, {
		data: 'Max drawdown',
		title: 'Max drawdown %',
		render: function(val) {
			return numeral(val).format('0,0.00');
		}
	}, {
		data: 'Days to recover',
		title: 'Days to recover',
		render: function(val) {
			return numeral(val).format('0,0');
		}
	}]
});
TabularTables.table_monthlyreturns = new Tabular.Table({
	name: 'table_monthlyreturns',
	collection: outputjsondt,
	selector: function(userId) {
		return {
			'name': 'table_monthlyreturns',
			'portfolioowner': userId
		};
	},
	order: [
		[0, "desc"]
	],
	paging: false,
	limit: 100,
	info: false,
	searching: false,
	responsive: true,
	autoWidth: false,
	columns: [{
		data: 'Year',
		title: 'Year'
	}, {
		data: 'Jan',
		title: 'Jan',
		render: function(val) {
			return numeral(val).format('0,0.00');
		}
	}, {
		data: 'Feb',
		title: 'Feb',
		render: function(val) {
			return numeral(val).format('0,0.00');
		}
	}, {
		data: 'Mar',
		title: 'Mar',
		render: function(val) {
			return numeral(val).format('0,0.00');
		}
	}, {
		data: 'Apr',
		title: 'Apr',
		render: function(val) {
			return numeral(val).format('0,0.00');
		}
	}, {
		data: 'May',
		title: 'May',
		render: function(val) {
			return numeral(val).format('0,0.00');
		}
	}, {
		data: 'Jun',
		title: 'Jun',
		render: function(val) {
			return numeral(val).format('0,0.00');
		}
	}, {
		data: 'Jul',
		title: 'Jul',
		render: function(val) {
			return numeral(val).format('0,0.00');
		}
	}, {
		data: 'Aug',
		title: 'Aug',
		render: function(val) {
			return numeral(val).format('0,0.00');
		}
	}, {
		data: 'Sep',
		title: 'Sep',
		render: function(val) {
			return numeral(val).format('0,0.00');
		}
	}, {
		data: 'Oct',
		title: 'Oct',
		render: function(val) {
			return numeral(val).format('0,0.00');
		}
	}, {
		data: 'Nov',
		title: 'Nov',
		render: function(val) {
			return numeral(val).format('0,0.00');
		}
	}, {
		data: 'Dec',
		title: 'Dec',
		render: function(val) {
			return numeral(val).format('0,0.00');
		}
	}, {
		data: 'YTD',
		title: 'YTD',
		render: function(val) {
			return numeral(val).format('0,0.00');
		}
	}]
});
TabularTables.table_ap = new Tabular.Table({
	name: 'table_ap',
	collection: outputjsondt,
	selector: function(userId) {
		return {
			'name': 'table_ap',
			'portfolioowner': userId
		};
	},
	responsive: true,
	columnDefs: [{
		responsivePriority: 1,
		targets: 0
	}, {
		responsivePriority: 2,
		targets: -1
	}, {
		responsivePriority: 3,
		targets: -2
	}],
	autoWidth: false,
	columns: [{
		data: 'Ticker',
		title: 'Ticker'
	}, {
		data: 'Capital in',
		title: 'Capital in',
		render: function(val) {
			return numeral(val).format('0,0.00');
		}
	}, {
		data: 'Commissions',
		title: 'Commissions',
		render: function(val) {
			return numeral(val).format('0,0.00');
		}
	}, {
		data: 'Dividends',
		title: 'Dividends',
		render: function(val) {
			return numeral(val).format('0,0.00');
		}
	}, {
		data: 'Capital out',
		title: 'Capital out',
		render: function(val) {
			return numeral(val).format('0,0.00');
		}
	}, {
		data: 'MV',
		title: 'MV',
		render: function(val) {
			return numeral(val).format('0,0.00');
		}
	}, {
		data: 'P/L',
		title: 'P/L',
		render: function(val) {
			return numeral(val).format('0,0.00');
		}
	}, {
		data: 'P/L %',
		title: 'P/L %',
		render: function(val) {
			return numeral(val).format('0,0.00');
		}
	}],
});
TabularTables.table_transaction = new Tabular.Table({
	name: 'table_transaction',
	collection: transaction,
	selector: function(userId) {
		return {
			'portfolioowner': userId
		};
	},
	responsive: true,
	columnDefs: [{
		responsivePriority: 1,
		targets: 1
	}, {
		responsivePriority: 2,
		targets: -1
	}],
	autoWidth: false,
	order: [
		[0, "desc"]
	],
	columns: [{
		data: 'date',
		title: 'Date',
		render: function(val) {
			return moment(val).format('YYYY-MM-DD');
		}
	}, {
		data: 'ticker',
		title: 'Ticker'
	}, {
		data: 'quantity',
		title: 'Quantity',
		render: function(val) {
			return numeral(val).format('0,0.00');
		}
	}, {
		data: 'price',
		title: 'Price',
		render: function(val) {
			return numeral(val).format('0,0.00');
		}
	}, {
		data: 'commission',
		title: 'Commission',
		render: function(val) {
			return numeral(val).format('0,0.00');
		}
	}, {
		data: 'fxrate',
		title: 'FX Rate',
		render: function(val) {
			return numeral(val).format('0,0.0000');
		}
	}, {
		title: 'Actions',
		tmpl: Meteor.isClient && Template.transactionactions
	}]
});
TabularTables.table_portfolio = new Tabular.Table({
	name: 'table_portfolio',
	collection: portfolio,
	selector: function(userId) {
		return {
			'portfolioowner': userId
		};
	},
	responsive: true,
	columnDefs: [{
		responsivePriority: 1,
		targets: 1
	}, {
		responsivePriority: 2,
		targets: -1
	}],
	autoWidth: false,
	paging: false,
	limit: 100,
	info: false,
	searching: false,
	order: [
		[0, "asc"]
	],
	columns: [{
		data: 'name',
		title: 'Name'
	}, {
		data: 'p_navprice_end',
		title: 'NAV price per share',
		render: function(val) {
			return numeral(val).format('0,0.00');
		}
	}, {
		data: 'p_navshares_end',
		title: '# of shares',
		render: function(val) {
			return numeral(val).format('0,0');
		}
	}, {
		data: 'p_mv_end',
		title: 'NAV',
		render: function(val) {
			return numeral(val).format('0,0.00');
		}
	}, {
		title: 'Actions',
		tmpl: Meteor.isClient && Template.portfolioactions
	}]
});
TabularTables.table_securitiesdatabase = new Tabular.Table({
	name: 'table_securitiesdatabase',
	collection: profile,
	responsive: true,
	autoWidth: false,
	order: [
		[0, "desc"]
	],
	columns: [{
		data: 'name',
		title: 'Name'
	}, {
		data: 'ticker',
		title: 'Yahoo Ticker'
	},
	/*{
		data: 'BBGTicker',
		title: 'Bloomberg Ticker'
	}, {
		data: 'RIC',
		title: 'RIC Code'
		},
		{
		data: 'ISIN',
		title: 'ISIN'
	}, {
		data: 'SEDOL',
		title: 'SEDOL'}, {
		data: 'CUSIP',
		title: 'CUSIP'
	}, */{
		data: 'sector',
		title: 'Morning Star Sector'
	}, {
		data: 'industry',
		title: 'Morning Star Industry'
	}]
});