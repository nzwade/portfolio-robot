App.info({
name: 'Portfolio Robot',
version: "0.0.1",
author: 'Wade Magowan',
email: 'nzwade@gmail.com',
website: 'http://portfoliorobot.com'
});

App.icons({
// Generated using https://appicontemplate.com/

// IOS
'iphone_2x': 'resources/icons/ios/icon-60@2x.png',
'iphone_3x': 'resources/icons/ios/icon-60@3x.png',
'ipad': 'resources/icons/ios/icon-76.png',
'ipad_2x': 'resources/icons/ios/icon-76@2x.png',
'ipad_pro': 'resources/icons/ios/icon-167.png',
'ios_settings': 'resources/icons/ios/Icon-Small.png',
'ios_settings_2x': 'resources/icons/ios/Icon-Small@2x.png',
'ios_settings_3x': 'resources/icons/ios/Icon-Small@3x.png',
'ios_spotlight': 'resources/icons/ios/Icon-Small-40.png',
'ios_spotlight_2x': 'resources/icons/ios/Icon-Small-40@2x.png',

// Android
'android_mdpi': 'resources/icons/android/48.png',
'android_hdpi': 'resources/icons/android/72.png',
'android_xhdpi': 'resources/icons/android/96.png',
'android_xxxhdpi': 'resources/icons/android/192.png'

/*iphone_2x (120x120)
iphone_3x (180x180)
ipad (76x76)
ipad_2x (152x152)
ipad_pro (167x167)
ios_settings (29x29)
ios_settings_2x (58x58)
ios_settings_3x (87x87)
ios_spotlight (40x40)
ios_spotlight_2x (80x80)
android_mdpi (48x48)
android_hdpi (72x72)
android_xhdpi (96x96)
android_xxhdpi (144x144)
android_xxxhdpi (192x192)*/
});


App.launchScreens({
// Generated using http://apetools.webprofusion.com/tools/imagegorilla

// IOS
'iphone_2x': 'resources/screens/ios/Default@2x~iphone_640x960.png',
'iphone5': 'resources/screens/ios/Default-568h@2x~iphone_640x1136.png',
'iphone6': 'resources/screens/ios/Default-750@2x~iphone6-portrait_750x1334.png',
'iphone6p_portrait': 'resources/screens/ios/Default-1242@3x~iphone6s-portrait_1242x2208.png',
'iphone6p_landscape': 'resources/screens/ios/Default-1242@3x~iphone6s-landscape_2208x1242.png',
'ipad_portrait': 'resources/screens/ios/Default-Portrait~ipad_768x1024.png',
'ipad_portrait_2x': 'resources/screens/ios/Default-Portrait@2x~ipad_1536x2048.png',
'ipad_landscape': 'resources/screens/ios/Default-Landscape~ipad_1024x768.png',
'ipad_landscape_2x': 'resources/screens/ios/Default-Landscape@2x~ipad_2048x1536.png',

// Android
'android_mdpi_portrait': 'resources/screens/android/drawable-mdpi/screen.png',
'android_mdpi_landscape': 'resources/screens/android/drawable-land-mdpi/screen.png',
'android_hdpi_portrait': 'resources/screens/android/drawable-hdpi/screen.png',
'android_hdpi_landscape': 'resources/screens/android/drawable-land-hdpi/screen.png',
'android_xhdpi_portrait': 'resources/screens/android/drawable-xhdpi/screen.png',
'android_xhdpi_landscape': 'resources/screens/android/drawable-land-xhdpi/screen.png',
'android_xxhdpi_portrait': 'resources/screens/android/drawable-xxhdpi/screen.png',
'android_xxhdpi_landscape': 'resources/screens/android/drawable-land-xxhdpi/screen.png'

/*iphone_2x (640x960)
iphone5 (640x1136)
iphone6 (750x1334)
iphone6p_portrait (1242x2208)
iphone6p_landscape (2208x1242)
ipad_portrait (768x1024)
ipad_portrait_2x (1536x2048)
ipad_landscape (1024x768)
ipad_landscape_2x (2048x1536)
android_mdpi_portrait (320x470)
android_mdpi_landscape (470x320)
android_hdpi_portrait (480x640)
android_hdpi_landscape (640x480)
android_xhdpi_portrait (720x960)
android_xhdpi_landscape (960x720)
android_xxhdpi_portrait (1080x1440)
android_xxhdpi_landscape (1440x1080)*/
});