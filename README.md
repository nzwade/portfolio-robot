# Portfolio Robot
---
**This project is no longer maintained as Yahoo Finance have turned off their API and there is no other reliable free alternative.**

Portfolio Robot is a portable analytics platform that facilitates management of portfolio risk, performance, allocation and accounting.

## Features
---
- Multiple portfolios.
- Bulk transaction uploads.
- Asset Allocation.
    - By position, currency, sector and industry.
- Portfolio Analytics.
    - Return, volatility, sharpe, max drawdown and days to recover compared to your choice of benchmark index.
    - Returns also provided on a monthly basis since portfolio inception.
- Asset Returns.
    - After accounting for commissions, dividends, splits and capital movements.
- Currency impact and movements.
    - Impact of currency on asset and overall portfolio return.
    - Line chart to show relevant historical exchange rates.
- Financial data provided by Yahoo Finance and Quandl.

## Built with
---
- Bootstrap
- Meteor
- HighCharts
- DataTables
- Pandas
- Beautiful Soup
- MongoDB
- RabbitMQ
- Celery
- Docker

---
![Screenshot](Screenshot.jpg)