from __future__ import division  # Needed to fix 1/252 = 0 issue
import datetime as dt
import numpy as np
import pandas as pd
import urllib2
import csv
from bson.objectid import ObjectId
from bs4 import BeautifulSoup
import logging
import pymongo
import os

__author__ = 'wade'
# TODO replace urllib2 with requests
# TODO tidy up datetime tuple/string variable
# TODO improve date logic in tasks

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

db = pymongo.MongoClient(os.environ.get('MONGO_URL_PROD','mongodb://localhost:3001/')).meteor  # DOCKER MONGO

# Prepare and save DF for CSV downloads
def outputcsv_createandinsert(outputdataframe, outputname, header, index, portfolioobjectid, portfolioowner, startdate,
                              enddate):
    outputdataframe_csv = outputdataframe.to_csv(header=header, index=index)
    tobeinserted = {"_id": str(ObjectId()),
                    "name": outputname,
                    "portfolio": portfolioobjectid,
                    "portfolioowner": portfolioowner,
                    "startdate": startdate,
                    "enddate": enddate,
                    "entered": dt.datetime.utcnow(),
                    "enteredby": __name__,
                    "data": outputdataframe_csv}
    db.outputcsv.insert(tobeinserted)


# Prepare and save DF for DataTables
def outputjsondt_createandinsert(outputdataframe, outputname, portfolioobjectid, portfolioowner, startdate, enddate):
    # Create a new column for index to make JSON compatible with datatables
    outputdataframe[outputdataframe.index.name] = outputdataframe.index
    outputdataframe['_id'] = outputdataframe.apply(lambda x: str(ObjectId()), axis=1)
    outputdataframe['name'] = outputname
    outputdataframe['portfolio'] = portfolioobjectid
    outputdataframe['portfolioowner'] = portfolioowner
    outputdataframe['startdate'] = startdate
    outputdataframe['enddate'] = enddate
    outputdataframe['entered'] = dt.datetime.utcnow()
    outputdataframe['enteredby'] = __name__
    outputdataframe_dict = outputdataframe.to_dict(orient="records")
    db.outputjsondt.insert_many(outputdataframe_dict)


# Prepare and save DF for Highcharts
def outputjsonhc_createandinsert(outputdataframe, outputname, portfolioobjectid, portfolioowner, startdate, enddate):
    data = []
    for col in outputdataframe.columns:
        rows = []
        for index, value in zip(outputdataframe.index, outputdataframe[col]):
            if np.isnan(value) is True:
                continue
            rows.append([index, value])
        d = {"name": col,
             "data": rows}
        data.append(d)
    tobeinserted = {"_id": str(ObjectId()),
                    "name": outputname,
                    "portfolio": portfolioobjectid,
                    "portfolioowner": portfolioowner,
                    "startdate": startdate,
                    "enddate": enddate,
                    "entered": dt.datetime.utcnow(),
                    "enteredby": __name__,
                    "data": data}
    db.outputjsonhc.insert(tobeinserted)


# Testing Chart.js
# def outputjsonhc_createandinsert(outputdataframe, outputname, portfolioobjectid, portfolioowner, startdate, enddate):
#     labels = list(outputdataframe.index)
#     datasets = []
#     for name in outputdataframe:
#         d = {'label': name,
#             'data': list(outputdataframe[name]),
#              'fill': False,
#              'lineTension': 0.1,
#              'backgroundColor': "rgba(75,192,192,0.4)",
#              'borderColor': "rgba(75,192,192,1)",
#              'borderCapStyle': 'butt',
#              'borderDash': [],
#              'borderDashOffset': 0.0,
#              'borderJoinStyle': 'miter',
#              'pointBorderColor': "rgba(75,192,192,1)",
#              'pointBackgroundColor': "#fff",
#              'pointBorderWidth': 1,
#              'pointHoverRadius': 5,
#              'pointHoverBackgroundColor': "rgba(75,192,192,1)",
#              'pointHoverBorderColor': "rgba(220,220,220,1)",
#              'pointHoverBorderWidth': 2,
#              'pointRadius': 1,
#              'pointHitRadius': 10
#              }
#         datasets.append(d)
#
#     tobeinserted = {"_id": str(ObjectId()),
#                     "name": outputname,
#                     "portfolio": portfolioobjectid,
#                     "portfolioowner": portfolioowner,
#                     "startdate": startdate,
#                     "enddate": enddate,
#                     "entered": dt.datetime.utcnow(),
#                     "enteredby": os.path.basename(__file__),
#                     "data": {'labels': labels,
#                              'datasets': datasets}}
#     db.outputjsonhc.insert(tobeinserted)

def outputmisc_insert(data, outputname, portfolioobjectid, portfolioowner, startdate, enddate):
    tobeinserted = {"_id": str(ObjectId()),
                    "name": outputname,
                    "portfolio": portfolioobjectid,
                    "portfolioowner": portfolioowner,
                    "startdate": startdate,
                    "enddate": enddate,
                    "entered": dt.datetime.utcnow(),
                    "enteredby": __name__,
                    "data": data}
    db.outputmisc.insert(tobeinserted)


# For split adjusting quantities
# Quantity today = stock bought or sold today + stock yesterday * split pct change today
def splitadjust(row, tkr_split):
    global global_count, global_previousrow
    global_count = -1
    global_previousrow = None
    if global_count == -1:
        pass
    else:
        row = row + global_previousrow * tkr_split.iloc[global_count]
    global_previousrow = row
    global_count += 1
    return row


# For date adjusting DF's
def dateadjust_startandend(df, startdate, enddate, minus7fromstart):
    if minus7fromstart is True:
        df = df[df.index >= (startdate - dt.timedelta(days=7))]
        df = df[df.index <= enddate]
    else:
        df = df[df.index >= startdate]
        df = df[df.index <= enddate]
    return df


# Annualised return
def annualised_return(yearlypctchange):
    numberofyears = yearlypctchange.shape[0]
    annualised_return = yearlypctchange + 1
    annualised_return = annualised_return.product()
    annualised_return = (annualised_return ** (1 / numberofyears) - 1) * 100
    return annualised_return


# Annualised volatility
def annualised_volatility(dailypctchange):
    daycount = 252
    return dailypctchange.std(ddof=0) * daycount ** (1 / 2) * 100


# Sharpe
def annualised_sharpe(dailypctchange):
    # TODO use actual risk free rate and day count
    riskfreerate = 0.01
    daycount = 252
    excessdailyreturns = dailypctchange - (riskfreerate / daycount)
    return excessdailyreturns.mean() / excessdailyreturns.std(ddof=0) * daycount ** (1 / 2)


# Max drawdown percentage and duration
def drawdownandduration(historicalprices):
    highwatermark = [0]
    historicalprices_index = historicalprices.index

    drawdown = pd.Series(index=historicalprices_index)
    duration = pd.Series(index=historicalprices_index)
    drawdownpercentage = pd.Series(index=historicalprices_index)

    for t in range(1, len(historicalprices_index)):
        cur_highwatermark = max(highwatermark[t - 1], historicalprices[t])
        highwatermark.append(cur_highwatermark)
        drawdown[t] = highwatermark[t] - historicalprices[t]
        if drawdown[t] == 0:
            duration[t] = 0
        else:
            duration[t] = duration[t - 1] + 1
        drawdownpercentage[t] = historicalprices[t] / highwatermark[t] - 1
    return drawdownpercentage.min() * 100, duration.max()


# Yahoo Eq Profile Scraper
def get_yahoo_profile(ticker):
    logger.debug("Getting Yahoo profile for ticker: {}".format(ticker))
    yahooprofile_url1 = "http://finance.yahoo.com/d/quotes.csv?s={}&f=nxc4".format(ticker)
    logger.debug(yahooprofile_url1)
    name = exchange = currency = sector = industry = 'N/A'
    try:
        yahooprofiledata1 = urllib2.urlopen(yahooprofile_url1).readlines()
        yahooprofiledata1csv = csv.reader(yahooprofiledata1)
        for row in yahooprofiledata1csv:
            name = row[0]
            exchange = row[1]
            currency = row[2]
        if currency == 'N/A':
            raise Exception
        # For downloading sector and industry
        if ticker[0] != "^":  # Benchmarks don't have a sector and industry
            yahooprofile_url2 = "http://finance.yahoo.com/q/pr?s={}+Profile".format(ticker)
            logger.debug(yahooprofile_url2)
            yahooprofiledata2 = urllib2.urlopen(yahooprofile_url2)
            soup = BeautifulSoup(yahooprofiledata2)
            soup_tdyfnc_tabledata1 = soup.find_all("td", class_='yfnc_tabledata1')
            soup_tdyfnc_tablehead1 = soup.find_all("td", class_='yfnc_tablehead1')
            if soup_tdyfnc_tablehead1 is not None and soup_tdyfnc_tablehead1[1].string == 'Sector:':
                sector = soup_tdyfnc_tabledata1[1].string
                industry = soup_tdyfnc_tabledata1[2].string

        yahooprofile = {"_id": str(ObjectId()),
                        "ticker": ticker,
                        "name": name,
                        "exchange": exchange,
                        "currency": currency,
                        "sector": sector,
                        "industry": industry,
                        "entered": dt.datetime.utcnow(),
                        "enteredby": __name__}
    except Exception:
        shorterror = "Failed to download profile for ticker: {}".format(ticker)
        logger.error(shorterror, exc_info=True)
        return "FAILURE", shorterror
    else:
        return "SUCCESS", yahooprofile


# Yahoo Eq Prices
def get_yahoo_price(ticker, start_date_tuple, end_date_tuple):
    logger.debug("Getting Yahoo prices for ticker: {}".format(ticker))
    yahooprices_url = "http://ichart.finance.yahoo.com/table.csv?s={}&a={}&b={}&c={}&d={}&e={}&f={}".format(
        ticker, start_date_tuple[1] - 1, start_date_tuple[2], start_date_tuple[0], end_date_tuple[1] - 1,
        end_date_tuple[2], end_date_tuple[0])
    logger.debug(yahooprices_url)
    yahooprices = []
    try:
        yahoopricesdata = urllib2.urlopen(yahooprices_url).readlines()
        yahoopricesdatacsv = csv.reader(yahoopricesdata)
        next(yahoopricesdatacsv)  # Ignore the header
        for row in yahoopricesdatacsv:
            data = {"_id": str(ObjectId()),
                    "ticker": ticker,
                    "date": dt.datetime.strptime(row[0], '%Y-%m-%d'),
                    "open": float(row[1]),
                    "high": float(row[2]),
                    "low": float(row[3]),
                    "close": float(row[4]),
                    "volume": float(row[5]),
                    "adjclose": float(row[6]),
                    "entered": dt.datetime.utcnow(),
                    "enteredby": __name__}
            yahooprices.append(data)
    except Exception:
        shorterror = "Failed to download prices for ticker: {}".format(ticker)
        logger.error(shorterror, exc_info=True)
        return "FAILURE", shorterror
    else:
        return "SUCCESS", yahooprices


# Yahoo corporate actions
def get_yahoo_split_dividend(ticker, start_date_tuple, end_date_tuple):
    logger.debug("Getting Yahoo splits and dividends for ticker: {}".format(ticker))
    yahoo_url = "http://ichart.finance.yahoo.com/x?s={}&a={}&b={}&c={}&d={}&e={}&f={}&g=v&y=0&z=30000".format(
        ticker, start_date_tuple[1] - 1, start_date_tuple[2], start_date_tuple[0], end_date_tuple[1] - 1,
        end_date_tuple[2], end_date_tuple[0])
    logger.debug(yahoo_url)
    splitdata = []
    dividenddata = []
    try:
        yahoosplitsdividendsdata = urllib2.urlopen(yahoo_url).readlines()  # Ignore the header and footer
        yahoosplitsdividendsdatacsv = csv.reader(yahoosplitsdividendsdata)
        next(yahoosplitsdividendsdatacsv)  # Ignore the header

        for row in yahoosplitsdividendsdatacsv:
            if row[0] == "STARTDATE":  # Ignore the footer
                break
            if row[0] == 'SPLIT':
                row[2] = row[2].split(':')
                row[2] = float(row[2][0]) / float(row[2][1])
            data = {"_id": str(ObjectId()),
                    "ticker": ticker,
                    "date": dt.datetime.strptime(row[1], ' %Y%m%d'),
                    "value": float(row[2]),
                    "entered": dt.datetime.utcnow(),
                    "enteredby": __name__}
            if row[0] == 'DIVIDEND':
                dividenddata.append(data)
            elif row[0] == 'SPLIT':
                splitdata.append(data)
            else:
                raise Exception("Not DIVIDEND or SPLIT")
    except Exception:
        shorterror = "Failed to download dividends and splits for ticker: {}".format(ticker)
        logger.error(shorterror, exc_info=True)
        return "FAILURE", shorterror
    else:
        return "SUCCESS", splitdata, dividenddata


def create_currency(currencypair, startdate, enddate):
    logger.debug("Creating currency data for pair: {}".format(currencypair))
    multiplicationfactor = 1
    if currencypair == "GBPGBp":
        multiplicationfactor = 100
    currencydata = []
    date = startdate
    while date <= enddate:
        data = {"currencypair": currencypair,
                "_id": str(ObjectId()),
                "date": date,
                "close": 1 * multiplicationfactor,
                "high": 1 * multiplicationfactor,
                "low": 1 * multiplicationfactor,
                "entered": dt.datetime.utcnow(),
                "enteredby": __name__}
        currencydata.append(data)
        date = date + dt.timedelta(days=1)
    return currencydata


def get_quandl_currency(currencypair, startdate_string, enddate_string):
    logger.debug("Getting Quandl currency data for pair: {}".format(currencypair))
    multiplicationfactor = 1
    if currencypair[3:6] == "GBp":
        multiplicationfactor = 100
        currencypair[3:6] = "GBP"  # Temporarily changing ccy code for data pull as GBp not available
    quandl_url = "https://www.quandl.com/api/v3/datasets/CURRFX/{}.csv?start_date={}&end_date={}&api_key=xs1BzncZXQe6i64_6_fC".format(
        currencypair, startdate_string, enddate_string)
    logger.debug(quandl_url)
    currencydata = []
    try:
        quandldata = urllib2.urlopen(quandl_url).readlines()
        quandldatacsv = csv.reader(quandldata)
        next(quandldatacsv)  # Skip the header
        for row in quandldatacsv:
            if row[1] == '':
                close = 0
            else:
                close = float(row[1]) * multiplicationfactor
            if row[2] == '':
                high = 0
            else:
                high = float(row[2]) * multiplicationfactor
            if row[3] == '':
                low = 0
            else:
                low = float(row[3]) * multiplicationfactor
            data = {"currencypair": currencypair,
                    "_id": str(ObjectId()),
                    "date": dt.datetime.strptime(row[0], '%Y-%m-%d'),
                    "close": close,
                    "high": high,
                    "low": low,
                    "entered": dt.datetime.utcnow(),
                    "enteredby": __name__}
            currencydata.append(data)
    except Exception:
        shorterror = "Failed to download currency data for pair: {}".format(currencypair)
        logger.error(shorterror, exc_info=True)
        return "FAILURE"
    else:
        return "SUCCESS", currencydata
