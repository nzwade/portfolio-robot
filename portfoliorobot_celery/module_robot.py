import module_toolkit as mtk
import datetime as dt
import pandas as pd
import numpy as np
import logging
# import matplotlib.pyplot as plt # For testing purposes

__author__ = 'wade'
# TODO instead of using the date adjustment function, do the adjustments elsewhere
logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

def robotics(portfolioobjectid, startdate, enddate):
    logger.debug(
        "Robotics started for portfolio: {} start date: {} end date: {}".format(portfolioobjectid, startdate, enddate))
    try:
        # Delete existing calculation results
        mtk.db.outputcsv.delete_many({"portfolio": portfolioobjectid})
        mtk.db.outputjsondt.delete_many({"portfolio": portfolioobjectid})
        mtk.db.outputjsonhc.delete_many({"portfolio": portfolioobjectid})
        mtk.db.outputmisc.delete_many({"portfolio": portfolioobjectid})

        # Find portfolio owner
        portfolioowner = mtk.db.portfolio.find_one({'_id': portfolioobjectid})['portfolioowner']

        #################################################################
        # Build dataframes for calculations
        portfolio = mtk.db.portfolio.find({"_id": portfolioobjectid})
        portfolio_df = pd.DataFrame(list(portfolio))
        portfolio_df.set_index('_id', inplace=True)
        p_name = portfolio_df['name'][0]

        transaction = mtk.db.transaction.find({"portfolio": portfolioobjectid})
        transaction_df = pd.DataFrame(list(transaction))
        transaction_df.set_index('date', inplace=True)

        eqprofile = mtk.db.profile.find({"ticker": {"$in": list(transaction_df['ticker'])}})
        eqprofile_df = pd.DataFrame(list(eqprofile))
        eqprofile_df.set_index('ticker', inplace=True)
        eqprofile_df['currencypair'] = portfolio_df['currency'][0] + eqprofile_df['currency']

        bmprofile = mtk.db.profile.find({"ticker": {"$in": portfolio_df['benchmark'][0]}})
        bmprofile_df = pd.DataFrame(list(bmprofile))
        bmprofile_df.set_index('ticker', inplace=True)
        bmprofile_df['currencypair'] = portfolio_df['currency'][0] + bmprofile_df['currency']
        bmprofile_df['descriptivename'] = bmprofile_df['name'].str.cat(bmprofile_df.index, sep=" - ")

        allcurrencypairs = pd.concat([eqprofile_df['currencypair'], bmprofile_df['currencypair']])
        currencypairs = allcurrencypairs.unique().tolist()

        currency = mtk.db.currency.find({"currencypair": {"$in": currencypairs}})
        currency_df = pd.DataFrame(list(currency))
        currency_df = currency_df.pivot(index='date', columns='currencypair', values='close')

        eqpricedaily = mtk.db.pricedaily.find({"ticker": {"$in": eqprofile_df.index.tolist()}},
                                          {"close": 1, "date": 1, "ticker": 1, "_id": 0})
        eq_price = pd.DataFrame(list(eqpricedaily))
        eq_price = eq_price.pivot(index='date', columns='ticker', values='close')
        eq_price.fillna(method='ffill', inplace=True)

        bmpricedaily = mtk.db.pricedaily.find({"ticker": {"$in": bmprofile_df.index.tolist()}},
                                          {"close": 1, "date": 1, "ticker": 1, "_id": 0})
        bm_price = pd.DataFrame(list(bmpricedaily))
        bm_price = bm_price.pivot(index='date', columns='ticker', values='close')
        bm_price.fillna(method='ffill', inplace=True)

        eq_currency = currency_df[eqprofile_df['currencypair'].tolist()]
        eq_currency.columns = eqprofile_df.index.tolist()
        eq_currency = eq_currency.reindex_like(eq_price)

        bm_currency = currency_df[bmprofile_df['currencypair'].tolist()]
        bm_currency.columns = bmprofile_df.index.tolist()
        bm_currency = bm_currency.reindex_like(bm_price)

        dividend = mtk.db.dividend.find({"ticker": {"$in": eqprofile_df.index.tolist()}},
                                    {"value": 1, "date": 1, "ticker": 1, "_id": 0})
        dividend_list = list(dividend)
        if len(dividend_list) == 0:
            eq_dividend = pd.DataFrame().reindex_like(eq_price)
            eq_dividend.fillna(value=0, inplace=True)
        else:
            dividend_df = pd.DataFrame(dividend_list)
            dividend_df = dividend_df.pivot(index='date', columns='ticker', values='value')
            eq_dividend = dividend_df.reindex_like(eq_price)
            eq_dividend.fillna(value=0, inplace=True)

        split = mtk.db.split.find({"ticker": {"$in": eqprofile_df.index.tolist()}},
                              {"value": 1, "date": 1, "ticker": 1, "_id": 0})
        split_list = list(split)
        if len(split_list) == 0:
            eq_split = None
        else:
            split_df = pd.DataFrame(split_list)
            split_df = split_df.pivot(index='date', columns='ticker', values='value')
            eq_split = split_df.reindex_like(eq_price)
            eq_split.fillna(1, inplace=True)

        txn_quantity = transaction_df[['ticker', 'quantity']].pivot(columns='ticker', values='quantity')

        eq_quantity = txn_quantity.reindex_like(eq_price)
        eq_quantity.fillna(value=0, inplace=True)
        if eq_split is not None:
            eq_quantity = eq_quantity.apply(lambda row: mtk.splitadjust(row, eq_split), axis=1)
        else:
            eq_quantity.cumsum()

        txn_price = transaction_df[['ticker', 'price']]
        txn_price = txn_price.pivot(columns='ticker', values='price')

        txn_currency = transaction_df[['ticker', 'fxrate']]
        txn_currency = txn_currency.pivot(columns='ticker', values='fxrate')

        txn_commission = transaction_df[['ticker', 'commission']]
        txn_commission = txn_commission.pivot(columns='ticker', values='commission')

        #################################################################
        # Date adjustments
        # TODO tried to find a way to make this code not so repetitive with no luck :(
        # dflist = [eq_price, eq_currency, eq_quantity, eq_dividend, txn_quantity, txn_price, txn_currency, txn_commission]

        eq_price = mtk.dateadjust_startandend(eq_price, startdate, enddate, minus7fromstart=False)
        eq_currency = mtk.dateadjust_startandend(eq_currency, startdate, enddate, minus7fromstart=False)
        eq_quantity = mtk.dateadjust_startandend(eq_quantity, startdate, enddate, minus7fromstart=False)
        eq_dividend = mtk.dateadjust_startandend(eq_dividend, startdate, enddate, minus7fromstart=False)
        txn_quantity = mtk.dateadjust_startandend(txn_quantity, startdate, enddate, minus7fromstart=False)
        txn_price = mtk.dateadjust_startandend(txn_price, startdate, enddate, minus7fromstart=False)
        txn_currency = mtk.dateadjust_startandend(txn_currency, startdate, enddate, minus7fromstart=False)
        txn_commission = mtk.dateadjust_startandend(txn_commission, startdate, enddate, minus7fromstart=False)

        if eq_split is not None:
            eq_split = mtk.dateadjust_startandend(eq_split, startdate, enddate, minus7fromstart=False)

        bm_price = mtk.dateadjust_startandend(bm_price, startdate, enddate, minus7fromstart=True)
        bm_currency = mtk.dateadjust_startandend(bm_currency, startdate, enddate, minus7fromstart=True)

        #################################################################
        # Insert downloadable CSV data
        dfnamelist = ['eq_price', 'eq_quantity', 'eq_currency', 'eq_dividend', 'txn_quantity', 'txn_price',
                      'txn_currency', 'txn_commission', 'bm_price', 'bm_currency']
        dflist = [eq_price, eq_quantity, eq_currency, eq_dividend, txn_quantity, txn_price, txn_currency,
                  txn_commission, bm_price, bm_currency]
        for x, y in zip(dfnamelist, dflist):
            if y is not None:
                y.index.name = 'Date'
                mtk.outputcsv_createandinsert(y, x, True, True, portfolioobjectid, portfolioowner, startdate, enddate)

        #################################################################
        # Portfolio calculations
        # MV
        eq_mv_local = eq_quantity * eq_price
        eq_mv_base = eq_mv_local / eq_currency

        # Commissions
        eq_commission_local = txn_commission
        eq_commission_local = eq_commission_local.reindex_like(eq_mv_local)
        eq_commission_local.fillna(0, inplace=True)
        eq_commission_local_total = eq_commission_local.sum()
        eq_commission_base = eq_commission_local / txn_currency
        eq_commission_base_total = eq_commission_base.sum()

        # Dividends
        eq_dividend_local = eq_quantity * eq_dividend
        eq_dividend_local_total = eq_dividend_local.sum()
        eq_dividend_base = eq_dividend_local / eq_currency
        eq_dividend_base_total = eq_dividend_base.sum()

        # Transactions
        eq_txn_local = txn_price * txn_quantity
        eq_txn_local = eq_txn_local.reindex_like(eq_mv_local)
        eq_txn_local.fillna(0, inplace=True)
        eq_txn_in_local = eq_txn_local[eq_txn_local > 0]
        eq_txn_out_local = eq_txn_local[eq_txn_local < 0]
        eq_txn_in_local_total = eq_txn_in_local.sum()
        eq_txn_out_local_total = eq_txn_out_local.sum()

        eq_txn_base = eq_txn_local / txn_currency
        eq_txn_base.fillna(0, inplace=True)
        eq_txn_in_base = eq_txn_base[eq_txn_base > 0]
        eq_txn_out_base = eq_txn_base[eq_txn_base < 0]
        eq_txn_in_base_total = eq_txn_in_base.sum()
        eq_txn_out_base_total = eq_txn_out_base.sum()

        # Portfolio MV
        p_mv = eq_mv_base.sum(axis=1)
        p_mv.name = p_name
        p_mv_end = p_mv.tail(1).round(2)

        # Total capital movements
        # eq_capmovement_local = eq_txn_local.append(eq_commission_local).append(eq_dividend_local)
        # eq_capmovement_local = eq_capmovement_local.groupby(eq_capmovement_local.index).sum()
        eq_capmovement_base = eq_txn_base.append(eq_commission_base).append(eq_dividend_base)
        eq_capmovement_base = eq_capmovement_base.groupby(eq_capmovement_base.index).sum()
        p_capmovement = eq_capmovement_base.sum(axis=1)
        p_capmovement.name = p_name
        p_capmovement_pct = p_capmovement / (p_mv - p_capmovement) + 1
        p_capmovement_pct.fillna(value=1, inplace=True)
        p_capmovement_pct.iloc[0] = 1
        p_capmovement_cumpct = p_capmovement_pct.cumprod()

        # NAV
        p_navshares = p_capmovement.head(1)
        p_navshares = p_navshares.reindex_like(p_mv)

        p_navshares.fillna(method='ffill', inplace=True)

        p_navshares = p_navshares * p_capmovement_cumpct
        p_navshares_end = p_navshares.tail(1).round(0)

        p_navprice_daily = p_mv / p_navshares
        p_navprice_end = p_navprice_daily.tail(1).round(3)

        mtk.db.portfolio.update_one({"_id": portfolioobjectid}, {"$set": {
            "p_mv_end": p_mv_end[0],
            "p_navshares_end": p_navshares_end[0],
            "p_navprice_end": p_navprice_end[0],
            "startdate": startdate,
            "enddate": enddate,
            "updated": dt.datetime.utcnow(),
            "updatedby": __name__}})

        p_navprice_daily_pctchange = p_navprice_daily.pct_change()
        # For calculating pct on day 1 (assumes nav starts at 1)
        p_navprice_daily_pctchange.iloc[0] = p_navprice_daily.iloc[0] - 1
        p_navprice_daily_pctchange = p_navprice_daily_pctchange.to_frame()
        p_navprice_daily_cumpctchange = (p_navprice_daily_pctchange + 1).cumprod() - 1

        # NAV - Monthly

        p_navprice_monthly = p_navprice_daily.asfreq('M', method='ffill')
        # To put current NAV in place:
        if p_navprice_monthly.tail(1).index.month != enddate.month:
            p_navprice_monthly.append(p_navprice_daily.tail(1))
        # Check if portfolio is less than a month old
        if p_navprice_monthly.count() == 1:
            p_navprice_monthly_pctchange = p_navprice_monthly
        else:
            p_navprice_monthly_pctchange = p_navprice_monthly.pct_change()
        # Adjust return for first period (assumes nav starts at 1)
        p_navprice_monthly_pctchange.iloc[0] = p_navprice_monthly.iloc[0] - 1
        p_navprice_monthly_pctchange = p_navprice_monthly_pctchange.to_frame()

        # NAV - Yearly
        p_navprice_yearly = p_navprice_daily.asfreq('A', method='ffill')
        # To put current NAV in place
        if p_navprice_yearly.tail(1).index.year != enddate.year:
            p_navprice_yearly.append(p_navprice_daily.tail(1))
        # Check if portfolio is less than a year old
        if p_navprice_yearly.count() == 1:
            p_navprice_yearly_pctchange = p_navprice_yearly
        else:
            p_navprice_yearly_pctchange = p_navprice_yearly.pct_change()
        # Adjust return for first period (assumes nav starts at 1)
        p_navprice_yearly_pctchange.iloc[0] = p_navprice_yearly.iloc[0] - 1
        p_navprice_yearly_pctchange = p_navprice_yearly_pctchange.to_frame()
        p_navprice_yearly_pctchange['Year'] = p_navprice_yearly_pctchange.index.year
        p_navprice_yearly_pctchange.set_index('Year', inplace=True)

        # Benchmarks
        bm_price_base_daily = bm_price / bm_currency
        # Fill_method in pct_change defaults to 'pad' so this uses last known value. essential for bank holidays.
        bm_price_base_daily_pctchange = bm_price_base_daily.pct_change()
        bm_price_base_daily_pctchange = bm_price_base_daily_pctchange[bm_price_base_daily_pctchange.index >= startdate]

        # Benchmarks - Yearly
        bm_price_base_yearly = bm_price_base_daily.asfreq('A', method='ffill')
        if bm_price_base_yearly.tail(1).index.year != enddate.year:
            bm_price_base_yearly = bm_price_base_yearly.append(bm_price_base_daily.tail(1))

        bm_price_base_yearly_pctchange = bm_price_base_yearly.pct_change()
        bm_price_base_yearly_pctchange['Year'] = bm_price_base_yearly_pctchange.index.year
        bm_price_base_yearly_pctchange.set_index('Year', inplace=True)
        # TODO add value for first year
        bm_price_base_daily_cumpctchange = (bm_price_base_daily_pctchange[
                                                bm_price_base_daily_pctchange.index >= startdate] + 1).cumprod() - 1
        # TODO in local currency terms my benchmark return is different to yahoo finance but i have reviewed the numbers and believe we are correct and yahoo finance are wrong.

        p_bm_cumulativereturns = bm_price_base_daily_cumpctchange

        # TODO For some reason the column names are not in same order as profile_df tickers?
        p_bm_cumulativereturnscols = [bmprofile_df['descriptivename'][bm] for bm in p_bm_cumulativereturns]

        p_bm_cumulativereturns[p_name] = p_navprice_daily_cumpctchange
        p_bm_cumulativereturnscols.append(p_name)

        p_bm_cumulativereturns.columns = p_bm_cumulativereturnscols

        p_bm_cumulativereturns = (p_bm_cumulativereturns * 100).round(2)
        p_bm_cumulativereturns.fillna(method='ffill', inplace=True)

        mtk.outputjsonhc_createandinsert(p_bm_cumulativereturns, 'p_bm_cumulativereturns', portfolioobjectid,
                                         portfolioowner, startdate, enddate)
        mtk.outputcsv_createandinsert(p_bm_cumulativereturns, 'p_bm_cumulativereturns', True, True, portfolioobjectid,
                                      portfolioowner, startdate, enddate)

        #################################################################
        # Time weighted asset returns
        # TODO complete or get rid
        # (Today's ending MV - Yesterday's closing MV + Today's dividends - Net cash flows into portfolio) / Yesterday's closing MV
        # twr = (eq_mv_local - eq_mv_local.shift(1) + eq_dividend_local - eq_capmovement_local) / eq_mv_local.shift(1)
        # print(twr.head())
        # print(twr.tail())
        # twr_cum = twr.add(1).cumprod()
        # print twr_cum.tail()

        #################################################################
        # Asset Allocation table
        aa_eqquantity = eq_quantity.tail(1)
        aa_eqprice = eq_price.tail(1)
        aa_eqlocalmv = eq_mv_local.tail(1)
        aa_eqbasemv = eq_mv_base.tail(1)

        aa_pct = (aa_eqbasemv / p_mv.tail(1)[0] * 100)

        table_aa = aa_eqquantity.append(aa_eqprice).append(aa_eqlocalmv).append(aa_eqbasemv).append(aa_pct)
        table_aa.index = ["Quantity", "Price", "MV (local)", "MV (base)", "Allocation"]
        table_aa = table_aa.transpose()
        table_aa.index.name = 'Ticker'
        table_aa['Currency'] = eqprofile_df['currency']
        table_aa['Sector'] = eqprofile_df['sector']
        table_aa['Industry'] = eqprofile_df['industry']
        table_aa['Name'] = eqprofile_df['name']
        table_aa_columns = ['Name'] + table_aa.columns.tolist()[:-1]
        table_aa = table_aa[table_aa_columns]
        table_aa.sort_values("Allocation", ascending=False)
        table_aa = table_aa[table_aa['Allocation'] != 0]

        mtk.outputcsv_createandinsert(table_aa, 'table_aa', True, True, portfolioobjectid, portfolioowner, startdate,
                                      enddate)
        mtk.outputjsondt_createandinsert(table_aa, 'table_aa', portfolioobjectid, portfolioowner, startdate, enddate)

        # Create data for allocation pie charts
        requiredpies = [['Allocation', 'chart_allocation'], ['Currency', 'chart_currencyallocation'],
                        ['Sector', 'chart_sectorallocation'],
                        ['Industry', 'chart_industryallocation']]
        for pie in requiredpies:
            chart_pieallocation = pd.DataFrame(table_aa['Allocation'])
            if pie[0] == 'Allocation':
                chart_pieallocation.index = table_aa.index
            else:
                chart_pieallocation.index = table_aa[pie[0]]
            chart_pieallocation = chart_pieallocation.groupby(by=chart_pieallocation.index).sum()
            chart_pieallocation = chart_pieallocation.round(2)
            mtk.outputjsonhc_createandinsert(chart_pieallocation, pie[1], portfolioobjectid, portfolioowner, startdate,
                                             enddate)

        #################################################################
        # Monthly returns table

        p_navprice_monthly_pctchange['year'] = p_navprice_monthly_pctchange.index.year
        p_navprice_monthly_pctchange['month'] = p_navprice_monthly_pctchange.index.month

        table_monthlyreturns = []

        for year in range(startdate.year, enddate.year + 1):
            returns = p_navprice_monthly_pctchange[p_navprice_monthly_pctchange['year'] == year]
            returns.set_index('month', inplace=True)
            returns = returns.reindex([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12])
            returns.drop('year', axis=1, inplace=True)
            returns.columns = [year]
            table_monthlyreturns.append(returns)
        table_monthlyreturns = pd.concat(table_monthlyreturns, axis=1)
        table_monthlyreturns = table_monthlyreturns.transpose()

        p_navprice_monthly_pctchange.drop(['year', 'month'], axis=1, inplace=True)
        table_monthlyreturns['YTD'] = p_navprice_yearly_pctchange
        table_monthlyreturns = (table_monthlyreturns * 100).round(2)
        table_monthlyreturns.columns = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov',
                                        'Dec', 'YTD']
        table_monthlyreturns.index.name = 'Year'
        table_monthlyreturns.fillna(value=0, inplace=True)

        mtk.outputcsv_createandinsert(table_monthlyreturns, 'table_monthlyreturns', True, True, portfolioobjectid,
                                      portfolioowner, startdate, enddate)
        mtk.outputjsondt_createandinsert(table_monthlyreturns, 'table_monthlyreturns', portfolioobjectid,
                                         portfolioowner, startdate, enddate)

        #################################################################
        # Analytics table
        # todo correct the number of days used
        timeframetext = ['1 day return', '5 day return', '1 month return', '3 month return', '1 year return',
                         '3 year return', '5 year return']
        timeframedays = [1, 5, 22, 65, 260, 780, 1300]

        table_analytics = pd.DataFrame()
        for p_bm in [p_name] + bmprofile_df.index.tolist():
            if p_bm == p_name:
                daily_pctchange = p_navprice_daily_pctchange[p_bm]
                yearly_pctchange = p_navprice_yearly_pctchange[p_bm]
            else:
                daily_pctchange = bm_price_base_daily_pctchange[p_bm]
                yearly_pctchange = bm_price_base_yearly_pctchange[p_bm]
            table_analytics_row = pd.DataFrame()
            table_analytics_row['Portfolio / benchmark'] = [p_bm]
            for text, days in zip(timeframetext, timeframedays):
                table_analytics_row[text] = ((daily_pctchange.iloc[-days:] + 1).cumprod() - 1).iloc[-1] * 100
            table_analytics_row['YTD return'] = yearly_pctchange.iloc[-1] * 100
            table_analytics_row = table_analytics_row[['Portfolio / benchmark'] + timeframetext[:3] + ['YTD return'] + timeframetext[3:]]
            table_analytics_row['Annualised return'] = mtk.annualised_return(yearly_pctchange)
            table_analytics_row['Annualised volatility'] = mtk.annualised_volatility(daily_pctchange)
            table_analytics_row['Annualised sharpe'] = mtk.annualised_sharpe(daily_pctchange)
            table_analytics_row['Max drawdown'], table_analytics_row['Days to recover'] = mtk.drawdownandduration(
                daily_pctchange)
            table_analytics = table_analytics.append(table_analytics_row)

        table_analytics.set_index('Portfolio / benchmark', inplace=True)
        table_analytics = table_analytics.round(2)
        # print(table_analytics)
        mtk.outputcsv_createandinsert(table_analytics, 'table_analytics', True, True, portfolioobjectid, portfolioowner,
                                      startdate, enddate)
        mtk.outputjsondt_createandinsert(table_analytics, 'table_analytics', portfolioobjectid, portfolioowner,
                                         startdate, enddate)

        #################################################################
        # Asset Performance Table BASE
        table_ap_base = pd.DataFrame()
        table_ap_base['Capital in'] = eq_txn_in_base_total * -1
        table_ap_base['Commissions'] = eq_commission_base_total * -1
        table_ap_base['Dividends'] = eq_dividend_base_total
        table_ap_base['Capital out'] = eq_txn_out_base_total * -1
        table_ap_base['MV'] = aa_eqbasemv.transpose()
        table_ap_base.fillna(value=0, inplace=True)
        table_ap_base['P/L'] = table_ap_base.sum(axis=1)
        table_ap_base['P/L %'] = ((table_ap_base['MV'] + table_ap_base['Capital out'] + table_ap_base[
            'Dividends']) / abs(
            table_ap_base['Capital in'] + table_ap_base['Commissions']) - 1) * 100

        table_ap_base_total = pd.DataFrame(data=table_ap_base.sum(axis=0), columns=[p_name])
        table_ap_base_total = table_ap_base_total.transpose()
        table_ap_base_total['P/L %'] = ((table_ap_base_total['MV'] + table_ap_base_total['Capital out'] +
                                         table_ap_base_total[
                                             'Dividends']) / abs(
            table_ap_base_total['Capital in'] + table_ap_base_total['Commissions']) - 1) * 100
        table_ap_base = table_ap_base.append(table_ap_base_total)

        table_ap_base = table_ap_base.round(2)
        table_ap_base.index.name = 'Ticker'

        mtk.outputcsv_createandinsert(table_ap_base, 'table_ap', True, True, portfolioobjectid, portfolioowner,
                                      startdate, enddate)
        mtk.outputjsondt_createandinsert(table_ap_base, 'table_ap', portfolioobjectid, portfolioowner, startdate,
                                         enddate)

        #################################################################
        # Asset Performance Table LOCAL - Not currently showing on platform but can do later
        table_ap_local = pd.DataFrame()
        table_ap_local['Capital in'] = eq_txn_in_local_total * -1
        table_ap_local['Commissions'] = eq_commission_local_total * -1
        table_ap_local['Dividends'] = eq_dividend_local_total
        table_ap_local['Capital out'] = eq_txn_out_local_total * -1
        table_ap_local['MV'] = aa_eqlocalmv.transpose()
        table_ap_local.fillna(value=0, inplace=True)
        table_ap_local['P/L'] = table_ap_local.sum(axis=1)
        table_ap_local['P/L %'] = ((table_ap_local['MV'] + table_ap_local['Capital out'] + table_ap_local[
            'Dividends']) / abs(
            table_ap_local['Capital in'] + table_ap_local['Commissions']) - 1) * 100

        table_ap_local_total = pd.DataFrame(data=table_ap_local.sum(axis=0), columns=[p_name])
        table_ap_local_total = table_ap_local_total.transpose()
        table_ap_local_total['P/L %'] = ((table_ap_local_total['MV'] + table_ap_local_total['Capital out'] +
                                          table_ap_local_total[
                                              'Dividends']) / abs(
            table_ap_local_total['Capital in'] + table_ap_local_total['Commissions']) - 1) * 100
        table_ap_local = table_ap_local.append(table_ap_local_total)

        table_ap_local = table_ap_local.round(2)
        table_ap_local.index.name = 'Ticker'

        # mtk.outputcsv_createandinsert(table_ap_local, 'table_ap_local', True, True, portfolioobjectid, portfolioowner, startdate, enddate)
        # mtk.outputjsondt_createandinsert(table_ap_local, 'table_ap_local', portfolioobjectid, portfolioowner, startdate, enddate)

        ############################################################
        # Asset Performance Chart

        eq_dividend_base_cumsum = eq_dividend_base.fillna(value=0).cumsum()
        eq_commission_base_cumsum = eq_commission_base.reindex_like(eq_price).fillna(value=0).cumsum()
        eq_txn_out_cumsum = eq_txn_out_base.reindex_like(eq_price).fillna(value=0).cumsum()
        eq_txn_in_cumsum = eq_txn_in_base.reindex_like(eq_price).fillna(value=0).cumsum()

        eq_pnl_cumpct = (eq_mv_base + eq_dividend_base_cumsum + eq_txn_out_cumsum * -1) / abs(
            eq_commission_base_cumsum + eq_txn_in_cumsum) - 1
        for eq in eq_pnl_cumpct:
            if np.isnan(eq_pnl_cumpct[eq].iloc[0]):
                eq_pnl_cumpct[eq].iloc[0] = 0  # Adjustment for where stock wasn't purchased on first day of portfolio

        eq_pnl_cumpct.fillna(method='ffill', inplace=True)
        eq_pnl_cumpct = (eq_pnl_cumpct * 100).round(2)

        mtk.outputcsv_createandinsert(eq_pnl_cumpct, 'eq_pnl_cumpct', True, True, portfolioobjectid, portfolioowner,
                                      startdate, enddate)
        mtk.outputjsonhc_createandinsert(eq_pnl_cumpct, 'eq_pnl_cumpct', portfolioobjectid, portfolioowner, startdate,
                                         enddate)

        #################################################################
        # Performance due to currency movements chart
        chart_currencyimpact = pd.DataFrame()
        chart_currencyimpact['Local'] = table_ap_local['P/L %']
        chart_currencyimpact['FX'] = (table_ap_base['P/L %'] - table_ap_local['P/L %']).round(2)
        chart_currencyimpact['Base'] = table_ap_base['P/L %']
        chart_currencyimpact_xaxis = chart_currencyimpact.index.values.tolist()

        mtk.outputmisc_insert(chart_currencyimpact_xaxis, 'chart_currencyimpact_xaxis', portfolioobjectid, portfolioowner, startdate,
                              enddate)
        mtk.outputjsonhc_createandinsert(chart_currencyimpact, 'chart_currencyimpact', portfolioobjectid,
                                         portfolioowner, startdate, enddate)

        #################################################################
        # Currency movements chart
        chart_currencymovements = eq_currency.copy()
        # TODO For some reason the column names are not in same order as profile_df tickers?:
        chart_currencymovements_columns = [eqprofile_df['currencypair'][eq] for eq in chart_currencymovements]
        chart_currencymovements.columns = chart_currencymovements_columns

        chart_currencymovements = chart_currencymovements.groupby(chart_currencymovements.columns, axis=1).first()
        chart_currencymovements = chart_currencymovements.round(4)

        mtk.outputcsv_createandinsert(chart_currencymovements, 'chart_currencymovements', True, True, portfolioobjectid,
                                      portfolioowner, startdate, enddate)
        mtk.outputjsonhc_createandinsert(chart_currencymovements, 'chart_currencymovements', portfolioobjectid,
                                         portfolioowner, startdate, enddate)

    except Exception:
        shorterror = "Robotics failed please contact support"
        logger.error(shorterror, exc_info=True)
        return "FAILURE", shorterror
    else:
        logger.debug("Robotics complete for portfolio: {}".format(portfolioobjectid))
        return "SUCCESS"
