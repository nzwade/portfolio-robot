from __future__ import absolute_import # PRODUCTION
from portfoliorobot_celery.celery import app # PRODUCTION
import portfoliorobot_celery.module_toolkit as mtk # PRODUCTION
import portfoliorobot_celery.module_robot as mr # PRODUCTION
# import module_toolkit as mtk  # LOCAL
# import module_robot as mr  # LOCAL
import datetime as dt
import traceback
import logging

__author__ = 'wade'

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')


@app.task # PRODUCTION
def runall(portfolioobjectid):
    shorterror = None
    longerror = None
    try:
        # Set status to started
        logger.debug("Data preparation started for portfolio: {}".format(portfolioobjectid))
        mtk.db.portfolio.update_one({'_id': portfolioobjectid},
                                    {'$set': {'status': 'STARTED', 'shorterror': shorterror, 'longerror': longerror}})

        # Get info on portfolio
        portfolio = mtk.db.portfolio.find_one({"_id": portfolioobjectid})

        # Setup dates
        defaultdownloadstartdate = dt.datetime(2000, 1, 1)

        robotstartdate = robotenddate = dt.datetime.utcnow() - dt.timedelta(days=1)
        robotenddate_string = dt.datetime.strftime(robotenddate, '%Y-%m-%d')
        robotenddate_tuple = dt.datetime.timetuple(robotenddate)[0:3]
        earliesttransaction = mtk.db.transaction.find({'portfolio': portfolioobjectid}).sort('date', 1).limit(1)
        if earliesttransaction.count(with_limit_and_skip=False) == 0:
            shorterror = "Portfolio has no transactions"
            raise Exception()
        for x in earliesttransaction:
            robotstartdate = x['date']  # Cursor will have one item but still need to iterate.

        # Build securities list consisting of portfolio benchmarks and tickers.
        bm_set = set()
        for benchmark in portfolio['benchmark']:
            bm_set.add(benchmark)
        tkr_set = set()
        transactions = mtk.db.transaction.find({"portfolio": portfolioobjectid}, {"ticker": 1, "_id": 0})
        for ticker in transactions:
            tkr_set.add(ticker['ticker'])

        security_list = bm_set | tkr_set

        # Download profile for securities where it is missing
        yahooprofiles = []
        for ticker in security_list:
            if mtk.db.profile.find_one({"ticker": ticker}) is None:
                yahooprofile = mtk.get_yahoo_profile(ticker)
                if yahooprofile[0] == "FAILURE":
                    shorterror = yahooprofile[1]
                    raise Exception()
                yahooprofiles.append(yahooprofile[1])
        if yahooprofiles:
            mtk.db.profile.insert_many(yahooprofiles)

        # Download historical prices
        for ticker in security_list:
            downloadstartdate = defaultdownloadstartdate
            # Find date of most recently downloaded price (if any)
            if mtk.db.pricedaily.find_one({"ticker": ticker}) is not None:
                mostrecentpricedate = mtk.db.pricedaily.find({'ticker': ticker}).sort('date', -1).limit(1)
                for x in mostrecentpricedate:
                    downloadstartdate = x['date'] + dt.timedelta(days=1)
                if downloadstartdate > robotenddate:
                    continue
            downloadstartdate_tuple = dt.datetime.timetuple(downloadstartdate)[0:3]

            yahooprices = mtk.get_yahoo_price(ticker, downloadstartdate_tuple, robotenddate_tuple)
            if yahooprices[0] == "FAILURE":
                shorterror = yahooprices[1]
                raise Exception()
            if yahooprices[1]:
                mtk.db.pricedaily.insert_many(yahooprices[1])

            # Download splits and dividends
            if ticker[0] != "^":  # To exclude benchmarks
                yahoosplitsdividends = mtk.get_yahoo_split_dividend(ticker, downloadstartdate_tuple, robotenddate_tuple)
                if yahoosplitsdividends[0] == "FAILURE":
                    shorterror = yahoosplitsdividends[1]
                    raise Exception()
                if yahoosplitsdividends[1]:
                    mtk.db.split.insert_many(yahoosplitsdividends[1])
                if yahoosplitsdividends[2]:
                    mtk.db.dividend.insert_many(yahoosplitsdividends[2])

        # Find all currencypairs
        currencypair_set = set()
        for ticker in security_list:
            security_currency = mtk.db.profile.find_one({"ticker": ticker}, {"currency": 1, "_id": 0})['currency']
            currencypair_set.add(portfolio['currency'] + security_currency)

        for currencypair in currencypair_set:
            downloadstartdate = defaultdownloadstartdate
            # Find date of most recently downloaded FX rates (if any)
            currency = mtk.db.currency.find({"currencypair": currencypair})
            if currency is not None:
                mostrecentcurrencydate = currency.sort('date', -1).limit(1)
                for x in mostrecentcurrencydate:
                    downloadstartdate = x['date'] + dt.timedelta(days=1)
                if downloadstartdate > robotenddate:
                    continue
            downloadstartdate_string = dt.datetime.strftime(downloadstartdate, '%Y-%m-%d')

            # Create FX rates for GBPGBp and same ccy pairs e.g. NZDNZD
            if currencypair == portfolio['currency'] + portfolio['currency'] or currencypair == "GBPGBp":
                createdcurrency = mtk.create_currency(currencypair, downloadstartdate, robotenddate)
                if createdcurrency:
                    mtk.db.currency.insert_many(createdcurrency)
            # Download FX rates from Quandl
            else:
                quandlcurrency = mtk.get_quandl_currency(currencypair, downloadstartdate_string, robotenddate_string)
                if quandlcurrency[0] == "FAILURE":
                    shorterror = quandlcurrency[1]
                    raise Exception()
                if quandlcurrency[1]:
                    mtk.db.currency.insert_many(quandlcurrency[1])

        logger.debug("Data preparation complete for portfolio: {}".format(portfolioobjectid))

        # Execute robotics
        roboticsresult = mr.robotics(portfolioobjectid, robotstartdate, robotenddate)
        if roboticsresult[0] == "FAILURE":
            shorterror = roboticsresult[1]
            raise Exception()

        status = "SUCCESS"
        mtk.db.portfolio.update_one({'_id': portfolioobjectid},
                                    {'$set': {'startdate': robotstartdate, 'enddate': robotenddate, 'status': status}})
    except Exception:
        status = "FAILURE"
        if shorterror is None:
            shorterror = "Data preparation failed please contact support"
            logger.error(shorterror, exc_info=True)
        mtk.db.portfolio.update_one({'_id': portfolioobjectid}, {
            '$set': {'startdate': robotstartdate, 'enddate': robotenddate, 'status': status, 'shorterror': shorterror,
                     'longerror': traceback.format_exc()}})


# runall("HvWf66wwmvxsJPPw2")
