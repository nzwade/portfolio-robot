from __future__ import absolute_import
from celery import Celery
import os

# from celery.schedules import crontab

app = Celery('portfoliorobot_celery',
             broker=os.environ.get('CELERY_BROKER_URL_PROD', 'amqp://guest:guest@localhost:5672'),
             backend='amqp://',
             include=['portfoliorobot_celery.tasks'])
# Use localhost instead of rabbitmq when not using Docker

app.conf.update(
    CELERY_RESULT_BACKEND='amqp',
    CELERY_RESULT_SERIALIZER='json',
    CELERY_TASK_SERIALIZER='json',
    CELERY_ACCEPT_CONTENT=['json']
    # ,CELERYBEAT_SCHEDULE = {
    # 'dailydatadownload': {
    #     'task': 'portfoliorobot_celery.tasks.dailydatadownload',
    #     'schedule': crontab(minute=00,hour=03),  # day_of_week='tue,wed,thu,fri,sat'
    #     'args': (),
    # }
    # }
)

if __name__ == '__main__':
    app.start()
