#############################
# LINUX HOST SETUP

# Install Docker Engine and Compose

https://docs.docker.com/compose/install/

# SSH key setup (run on client)

cd ~/.ssh/
ssh-keygen -t rsa
ssh-add ~/.ssh/digitalocean

# Setup swap

sudo swapon -s
sudo dd if=/dev/zero of=/swapfile bs=1M count=1024
sudo chmod 600 /swapfile
sudo mkswap /swapfile
sudo swapon /swapfile

# All running processes

ps aux

# View processes running on specific port
lsof -i :80

#############################
# DOCKER

# Deploy all services to Remote Docker

# Build Meteor image locally to avoid memory error.
# Requires pipeviewer: (brew install pv)
docker save nzwade/portfoliorobot_meteor | bzip2 | pv | ssh root@46.101.95.31 'bunzip2 | docker load'
cd '/Volumes/storage partition/dev/portfolio-robot'
scp -r . root@46.101.95.31:/root
ssh root@46.101.95.31 'docker-compose up -d'

# Backup Remote Docker Mongo DB:

ssh root@46.101.95.31 'rm dump -r'
ssh root@46.101.95.31 'docker exec mongo rm dump -r'
ssh root@46.101.95.31 'docker exec mongo mongodump'
ssh root@46.101.95.31 'docker cp mongo:dump .'
rm -r '/Volumes/storage partition/dev/portfolio-robot/dump'
scp -r root@46.101.95.31:dump '/Volumes/storage partition/dev/portfolio-robot/'

# Backup Local Meteor Mongo DB

rm -r '/Volumes/storage partition/dev/portfolio-robot/dump'
cd '/Volumes/storage partition/dev/portfolio-robot/'
mongodump -h 127.0.0.1 --port 27017 -d meteor

# Restore DB to Local Meteor Mongo:

cd '/Volumes/storage partition/dev/portfolio-robot/portfoliorobot_meteor'
meteor mongo
db.dropDatabase()
cd '/Volumes/storage partition/dev/portfolio-robot/dump/meteor'
mongorestore -h 127.0.0.1 --port 3001 --db meteor ./

# Restore DB to Remote Docker Mongo:

ssh root@46.101.95.31 'rm dump -r'
ssh root@46.101.95.31 'docker exec mongo rm dump -r'
scp -r '/Volumes/storage partition/dev/portfolio-robot/dump' root@46.101.95.31
ssh root@46.101.95.31 'docker cp dump mongo:/dump'
ssh root@46.101.95.31
docker exec mongo mongorestore --db meteor dump/meteor


cd '/Volumes/storage partition/dev/portfolio-robot/'
docker exec mongo rm dump -r
docker exec mongo mongorestore --db meteor dump/meteor

# List all running containers

docker ps

# Stop all containers

docker stop $(docker ps -a -q)

# Remove all containers

docker rm $(docker ps -a -q)

# Remove all images

docker rmi $(docker images | grep "^<none>" | awk "{print $3}")

# Detach from a container

Ctrl-p + Ctrl-q

# Build a container

docker build -t portfoliorobot_celery:latest ./portfoliorobot_celery

# Run a container

docker run -it portfoliorobot_celery

# Stop a container

docker stop portfoliorobot_celery

# View container logs

docker logs portfoliorobot_meteor
docker logs portfoliorobot_celery_1
docker exec portfoliorobot_celery_1 tail celeryd.log
docker exec -it portfoliorobot_celery_1 celery -A portfoliorobot_celery status
docker exec -it rabbitmq rabbitmqctl list_queues

# Open Bash on container

docker exec -it portfoliorobot_celery /bin/bash

# Mongo CLI

docker exec -it mongo mongo meteor

#############################
# METEOR

$ Run locally

CELERY_BROKER_URL='amqp://guest:guest@localhost:5672//' meteor

# Run locally with mobile app support

meteor run android-device --mobile-server=http://portfoliorobot.com --settings=settings.json CELERY_BROKER_URL='amqp://guest:guest@localhost:5672//'

# Open Android Studio

CELERY_BROKER_URL='amqp://guest:guest@localhost:5672//' meteor run android

# Check version

meteor --version

# Reset calculation

portfolio.update({_id: "HvWf66wwmvxsJPPw2"},{status: "reset"})

#############################
## MONGO

# Change email

db.users.update({"_id":"Hax6Rwk64zttTKCfN"}, {$set:{"emails.0.address":"portfoliorobot@portfoliorobot.com"}})

# Delete market data

db.profile.remove({})
db.pricedaily.remove({})
db.split.remove({})
db.dividend.remove({})
db.currency.remove({})

# View version

db.version()

# View Storage Engine

db.serverStatus().storageEngine

#############################
## ARCHIVE

# KADIRA

Kadira.connect('E3XPg9JjMLCpN489F', 'a10aee31-3c08-4640-8df5-fee265a2b33a');

# Problems with Meteor:

1. Difficult deployments
arunodamup -> arunodamupx -> kadiramup
kadirahq abandoned projects in favour of react storybook.
iron router not updated

# Import from CSV into Mongo

mongoimport --host localhost:3001 -d meteor -c transactions --type csv --file /home/transactions.csv --headerline

# VIRTUALENV SUPERVISORD and SUPERVISORCTL

source portfoliorobot_celery/venv/bin/activate
supervisord -c supervisord_prod.conf
supervisorctl -c supervisord_prod.conf

# MUP

Editted start.sh and install-mongodb.sh so they boot to right docker network
/usr/local/lib/node_modules/mup/lib/modules/meteor/assets/templates
/usr/local/lib/node_modules/mup/lib/modules/mongo/assets

# CHART.JS

added:

// < HACK >
if (!process.env.BROWSER) {
  global.window = {};
}
// </ HACK >

to

/Users/wademagowan/portfolio-robot/portfolio-robot/portfoliorobot_meteor/node_modules/chart.js/src/core.helpers.js

# YAHOO EXCHANGE CODES TO COUNTRIES
BUE Argentina
ASX Australia
NCM Australia
SHH Australia
VIE Austria
EBS Austria
BRU Belgium
SAO Brazil
VAN Canada
PAR France
BER Germany
DUS Germany
FRA Germany
HAM Germany
HAN Germany
MUN Germany
STU Germany
CBT Germany
EUX Germany
ATH Greece
HKG Hong Kong
MCX India
JKT Indonesia
VTX International
MIL Italy
TOR Italy
TLV Izrael
KSC Korea
KLS Malaysia
MEX Mexico
AMS Netherlands
NZE New Zealand
OSL Norway
LIS Portugal
BSE Romania
SES Singapore
MAD Spain
MCE Spain
NGM Sweeden
STO Sweeden
TAI Taiwan
TWO Taiwan
BTS Thailand
ISE UK
IOB UK
LSE UK
ASE USA
NAS USA
NMS USA
OBB USA
PCX USA
CPH Denmark
GER Germany
